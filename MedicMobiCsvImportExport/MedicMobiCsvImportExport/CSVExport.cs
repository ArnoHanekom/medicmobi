﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace MedicMobiCsvImportExport
{
    class CSVExport
    {
        public void CSVExp(string csvPath, DataTable results)
        {
            exportCSV(csvPath, results);
        }

        private void exportCSV(string csvPath, DataTable results)
        {
            FileInfo fi = new FileInfo(csvPath);
            string outputName = fi.Name.Substring(0, fi.Name.Length - 4);
            outputName += "_Output.csv";
            using (StreamWriter sw = new StreamWriter(fi.Directory + @"\" + outputName))
            {
                foreach(DataRow dr in results.Rows)
                {
                    sw.WriteLine("{0},{1},{2:0.##}", dr[0].ToString(), dr[1].ToString(), decimal.Round(decimal.Parse(dr[2].ToString()), 2));
                }
            }
        }
    }
}
