﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MedicMobiCsvImportExport
{
    class Program
    {
        static void Main(string[] args)
        {
            CSVImportAndCalculation importCSV = new CSVImportAndCalculation();
            appDescription();

            Console.WriteLine("Please enter CSV path: ");
            string csvPath = Console.ReadLine();
            
            while(!csvPath.EndsWith(".csv") || !File.Exists(csvPath))
            {
                if(!File.Exists(csvPath))                
                    Console.WriteLine("File does not exist, please enter a valid file path: ");
                else if(!csvPath.EndsWith(".csv"))
                    Console.WriteLine("File is not a valid CSV file: ");

                csvPath = Console.ReadLine();
            }

            importCSV.getCSV(csvPath);

            Console.WriteLine("");
            if (File.Exists(csvPath))
            {
                FileInfo fi = new FileInfo(csvPath);
                string outputName = fi.Name.Substring(0, fi.Name.Length - 4);
                outputName += "_Output.csv";
                Console.WriteLine("Results exported to: " + fi.Directory + @"\" + outputName);
            }
                
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        static void appDescription()
        {
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**     Medic Mobi Test Application Description and Instructions     **");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("** This application is written in C#.Net.                           **");
            Console.WriteLine("** Compile application using Visual Studio 2015 or simply run       **");
            Console.WriteLine("** attached MedicMobiCsvImportExport.exe.                           **");
            Console.WriteLine("**                                                                  **");
            Console.WriteLine("** This application will take an input of an existing,              **");
            Console.WriteLine("** comma-delimited CSV file with 3 columns                          **");
            Console.WriteLine("** (personA, personB and amount) and output a summerized CSV file   **");
            Console.WriteLine("** calculated in the following manner:                              **");
            Console.WriteLine("**      PersonA owes personB x amount, the amount will be rounded   **");
            Console.WriteLine("**      to the second decimal number.                               **");
            Console.WriteLine("**      The summerized output will be ordered alphabetically based  **");
            Console.WriteLine("**      on the first column (personA)                               **");
            Console.WriteLine("**                                                                  **");
            Console.WriteLine("** Errors will be logged to a txt file and location of this file    **");
            Console.WriteLine("** will be displayed to the user after error is logged and the      **");
            Console.WriteLine("** application will quit.                                           **");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("");
            Console.WriteLine("");
        }
    }
}
