﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;


namespace MedicMobiCsvImportExport
{
    class CSVImportAndCalculation
    {
        public void getCSV(string csvPath)
        {
            loadCSV(csvPath);
        }        

        private void loadCSV(string csvPath)
        {
            DataTable resultDT = new DataTable();
            resultDT.Columns.Add("personA");
            resultDT.Columns.Add("personB");
            resultDT.Columns.Add("amount");
            float recordCounter = 0;
            try
            {
                using (StreamReader sr = new StreamReader(csvPath))
                {
                    while (!sr.EndOfStream)
                    {
                        recordCounter++;
                        Console.Write("\r{0} Records Processed   ", recordCounter);
                        string line = sr.ReadLine();
                        string[] values = line.Split(',');
                        decimal lineAmount = 0;

                        try
                        {
                            lineAmount = decimal.Parse(values[2]);
                        }
                        catch (Exception ex)
                        {
                            using (StreamWriter sw = File.AppendText(System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt"))
                            {
                                sw.WriteLine("{0} - Value: '{1}' - Error - {2}", DateTime.Now, values[2].ToString(), ex.Message);
                            }

                            Console.WriteLine("");
                            Console.WriteLine("An error occurred, please see log file for more info: " + System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt");
                            Console.ReadLine();
                            Environment.Exit(0);
                        }

                        bool inMain = false;

                        foreach (DataRow dr in resultDT.Rows)
                        {
                            if (dr[0].ToString().Trim().ToLower() == values[0].ToString().Trim().ToLower() && dr[1].ToString().Trim().ToLower() == values[1].ToString().Trim().ToLower())
                            {
                                inMain = true;
                                decimal currAmount = 0;

                                try
                                {
                                    currAmount = decimal.Parse(dr[2].ToString());
                                }
                                catch (Exception ex)
                                {
                                    using (StreamWriter sw = File.AppendText(System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt"))
                                    {
                                        sw.WriteLine("{0} - Value: '{1}' - Error - {2}", DateTime.Now, dr[2].ToString(), ex.Message);
                                    }

                                    Console.WriteLine("");
                                    Console.WriteLine("An error occurred, please see log file for more info: " + System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt");
                                    Console.ReadLine();
                                    Environment.Exit(0);
                                }

                                currAmount = currAmount + lineAmount;
                                dr[2] = currAmount;
                                break;
                            }
                        }

                        if (!inMain)
                        {
                            object[] obj = new object[] { values[0], values[1], values[2] };
                            resultDT.Rows.Add(obj);
                        }
                    }
                }

                resultDT.DefaultView.Sort = resultDT.Columns[0].ColumnName + " ASC";
                resultDT = resultDT.DefaultView.ToTable();

                CSVExport csvexport = new CSVExport();
                csvexport.CSVExp(csvPath, resultDT);
            }
            catch(Exception ex)
            {
                using (StreamWriter sw = File.AppendText(System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt"))
                {
                    sw.WriteLine("{0} - Error - {1}", DateTime.Now, ex.Message);
                }

                Console.WriteLine("");
                Console.WriteLine("An error occurred, please see log file for more info: " + System.IO.Directory.GetCurrentDirectory() + @"\ErrorLog.txt");
                Console.ReadLine();
                Environment.Exit(0);
            }
        }
    }
}
